#!/usr/bin/env bash

set -e

pnpm install
pnpm upgrade --latest
pnpm-deduplicate
pnpm prune
pnpm-deduplicate
pnpm outdated

exit 0
