#!/usr/bin/env bash

set -e

yarn install
yarn upgrade --latest
yarn-deduplicate --strategy=highest yarn.lock
yarn outdated

exit 0
