#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -yq --no-install-recommends openssh-server
apt-get autoremove -y
apt-get clean -y
rm -rf /var/lib/apt/lists/*

mkdir -p -m0755 /var/run/sshd
