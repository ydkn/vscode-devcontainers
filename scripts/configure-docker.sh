#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

groupadd -g 800 docker
usermod -a -G docker "${DEVC_USER}"

DEBIAN_FRONTEND=noninteractive

source /etc/os-release

curl -fsSL https://download.docker.com/linux/${ID}/gpg | gpg --dearmor >/etc/apt/trusted.gpg.d/docker-keyring.gpg
echo "deb https://download.docker.com/linux/${ID} ${VERSION_CODENAME} stable" >/etc/apt/sources.list.d/docker.list

apt-get update
apt-get install -yq --no-install-recommends docker-ce-cli
apt-get autoremove -y
apt-get clean -y
rm -rf /var/lib/apt/lists/*
