#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

groupadd -r golang
usermod -a -G golang "${DEVC_USER}"

version=${GO_VERSION:-"latest"}
arch=$(dpkg --print-architecture)

if [ "${version}" == "latest" ]; then
  version=$(git ls-remote --tags "https://go.googlesource.com/go" | grep -oP "refs\/tags\/go\d+(\.\d+)+$" | awk -F'refs/tags/go' '{print $2}' | sort -rV | head -n 1)
fi

mkdir /usr/local/go

curl -fsSL -o /tmp/go.tar.gz "https://golang.org/dl/go${version}.linux-${arch}.tar.gz"
tar -xzf /tmp/go.tar.gz -C /usr/local/go --strip-components=1

mkdir /go

# install tools
mkdir -p /tmp/gotools
export PATH="/usr/local/go/bin:${PATH}"
export GOPATH=/tmp/gotools
export GOCACHE=/tmp/gotools/cache
go install "github.com/golangci/golangci-lint/cmd/golangci-lint@latest"
mv /tmp/gotools/bin/* /usr/local/go/bin/

rm -rf /tmp/*

chgrp -R golang /usr/local/go /go
chmod -R g+r+w /usr/local/go /go

# Install bash configuration
echo 'export PATH="/go/bin:/usr/local/go/bin:${PATH}"' >>/etc/bash.bashrc
echo 'export GOPATH=/go' >>/etc/bash.bashrc

# Install zsh configuration
echo 'export PATH="/go/bin:/usr/local/go/bin:${PATH}"' >>/etc/zsh/zshrc
echo 'export GOPATH=/go' >>/etc/zsh/zshrc
