#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

touch "${DEVC_HOME}/.bashrc" "${DEVC_HOME}/.zshrc"
chown "${DEVC_UID}:${DEVC_GID}" "${DEVC_HOME}/.bashrc" "${DEVC_HOME}/.zshrc"

# bash
# ====

bash_promt="$(
  cat <<'EOS'
__bash_prompt() {
    local userpart='$(export XIT="$?" && echo -n "\[\033[0;32m\]\u " && \
      [ "$XIT" -ne "0" ] && echo -n "\[\033[1;31m\]➜" || echo -n "\[\033[0m\]➜")'
    local gitbranch='$(
    if [ "$(git config --get codespaces-theme.hide-status 2>/dev/null)" != 1 ]; then
      export BRANCH=$(git symbolic-ref --short HEAD 2>/dev/null || git rev-parse --short HEAD 2>/dev/null)
      if [ "${BRANCH}" != "" ]; then
        echo -n "\[\033[0;36m\](\[\033[1;31m\]${BRANCH}" &&
          if git ls-files --error-unmatch -m --directory --no-empty-directory -o --exclude-standard ":/*" >/dev/null 2>&1; then
            echo -n " \[\033[1;33m\]✗"
          fi &&
          echo -n "\[\033[0;36m\]) "
      fi
    fi
  )'
    local lightblue='\[\033[1;34m\]'
    local removecolor='\[\033[0m\]'
    PS1="${userpart} ${lightblue}\w ${gitbranch}${removecolor}\$ "
    unset -f __bash_prompt
}
__bash_prompt
EOS
)"

echo "${bash_promt}" >>"${DEVC_HOME}/.bashrc"

echo '[ -f "${HOME}/.bashrc.local" ] && source "${HOME}/.bashrc.local"' >>"${DEVC_HOME}/.bashrc"

# zsh
# ===

# powerlevel10k
sudo git clone --depth=1 \
  -c core.eol=lf \
  -c core.autocrlf=false \
  -c fsck.zeroPaddedFilemode=ignore \
  -c fetch.fsck.zeroPaddedFilemode=ignore \
  -c receive.fsck.zeroPaddedFilemode=ignore \
  https://github.com/ohmyzsh/ohmyzsh /usr/local/share/ohmyzsh

echo -e "$(cat "/usr/local/share/ohmyzsh/templates/zshrc.zsh-template")\nDISABLE_AUTO_UPDATE=true\nDISABLE_UPDATE_PROMPT=true" >>"${DEVC_HOME}/.zshrc"
sed -i -e 's/export ZSH=.*/export ZSH="\/usr\/local\/share\/ohmyzsh"/g' "${DEVC_HOME}/.zshrc"

zsh_promt="$(
  cat <<'EOF'
__zsh_prompt() {
    local prompt_username="%n"
    PROMPT="%{$fg[green]%}${prompt_username} %(?:%{$reset_color%}➜ :%{$fg_bold[red]%}➜ )" # User/exit code arrow
    PROMPT+='%{$fg_bold[blue]%}%(5~|%-1~/…/%3~|%4~)%{$reset_color%} ' # cwd
    PROMPT+='$([ "$(git config --get codespaces-theme.hide-status 2>/dev/null)" != 1 ] && git_prompt_info)' # Git status
    PROMPT+='%{$fg[white]%}$ %{$reset_color%}'
    unset -f __zsh_prompt
}
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[cyan]%}(%{$fg_bold[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg_bold[yellow]%}✗%{$fg_bold[cyan]%})"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[cyan]%})"
__zsh_prompt
EOF
)"

echo "${zsh_promt}" >>"${DEVC_HOME}/.zshrc"

echo '[ -f "${HOME}/.zshrc.local" ] && source "${HOME}/.zshrc.local"' >>"${DEVC_HOME}/.zshrc"
