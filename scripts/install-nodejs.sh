#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

groupadd -r nodejs
usermod -a -G nodejs "${DEVC_USER}"

DEBIAN_FRONTEND=noninteractive

curl -fsSL https://deb.nodesource.com/setup_${NODEJS_VERSION}.x | bash -

apt-get update
apt-get install -yq --no-install-recommends nodejs
apt-get autoremove -y
apt-get clean -y
rm -rf /var/lib/apt/lists/*

umask 0002

npm install --global typescript yarn yarn-deduplicate pnpm pnpm-deduplicate

chgrp -R nodejs /usr/lib/node_modules
chmod -R g+r+w /usr/lib/node_modules

# cleanup
rm -rf "${HOME}/.npm"
rm -rf "${DEVC_HOME}/.npm"
