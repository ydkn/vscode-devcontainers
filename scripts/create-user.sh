#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

groupadd --gid "${DEVC_GID}" "${DEVC_USER}"
useradd -s /usr/bin/zsh -d "${DEVC_HOME}" -u "${DEVC_UID}" -g "${DEVC_GID}" -m "${DEVC_USER}"

echo "${DEVC_USER} ALL=(root) NOPASSWD:ALL" >"/etc/sudoers.d/${DEVC_USER}"
chmod 0440 "/etc/sudoers.d/${DEVC_USER}"

sudo -u "${DEVC_USER}" mkdir -p "${DEVC_HOME}/.local/bin"
