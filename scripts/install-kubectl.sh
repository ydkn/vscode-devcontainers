#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

arch=$(dpkg --print-architecture)
version=$(curl -sL https://dl.k8s.io/release/stable.txt)

curl -sL "https://dl.k8s.io/release/${version}/bin/linux/${arch}/kubectl" -o /usr/local/bin/kubectl
chmod +x /usr/local/bin/kubectl

# Install bash configuration
echo "source <(kubectl completion bash)" >>/etc/bash.bashrc
echo "alias k=kubectl" >>/etc/bash.bashrc
echo "complete -F __start_kubectl k" >>/etc/bash.bashrc

# Install zsh configuration
echo "source <(kubectl completion zsh)" >>/etc/zsh/zshrc
echo "alias k=kubectl" >>/etc/zsh/zshrc
