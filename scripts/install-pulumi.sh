#!/usr/bin/env bash

set -e

source /etc/devcontainer.d/build.env

sudo -u "${DEVC_USER}" sh -c "cd /tmp && curl -fsSL https://get.pulumi.com | sh"

# Install bash configuration
echo 'export PATH="${HOME}/.pulumi/bin:${PATH}"' >>/etc/bash.bashrc
echo 'alias p="pulumi"' >>/etc/bash.bashrc

# Install zsh configuration
echo 'export PATH="${HOME}/.pulumi/bin:${PATH}"' >>/etc/zsh/zshrc
echo 'alias p=pulumi' >>/etc/zsh/zshrc
